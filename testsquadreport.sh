#!/bin/bash -ex

apt-get update && apt-get install -y wget curl git virtualenv procps

cat /proc/meminfo
echo "=============="
cat /proc/cpuinfo

free -h
df -h 

dir_old=$(pwd)
echo "===workaround start with swap==="
dd if=/dev/zero of=swapfile2 bs=4M count=1024
chmod 600 swapfile2
mkswap swapfile2
swapon swapfile2

free -h
df -h

echo "===workaround end with swap==="


qareport_url="https://qa-reports.linaro.org"
LKFT_WORK_DIR="${PWD}"
virtualenv --python=python3 "${LKFT_WORK_DIR}/workspace"
source ${LKFT_WORK_DIR}/workspace/bin/activate
[ ! -d ${LKFT_WORK_DIR}/squad-client ] && git clone --depth 1 https://github.com/liuyq/squad-client "${LKFT_WORK_DIR}/squad-client"
cd "${LKFT_WORK_DIR}/squad-client" && pip install -r requirements.txt && python setup.py install && cd -
[ ! -d ${LKFT_WORK_DIR}/squad-report ] && git clone --depth 1 https://gitlab.com/Linaro/lkft/users/yongqin.liu/squad-report.git "${LKFT_WORK_DIR}/squad-report"
cd "${LKFT_WORK_DIR}/squad-report" && pip install -r requirements.txt && python setup.py install && cd -

report_kernel="android12-510-db845c-presubmit"
qareport_build="5.10.110-98fa7f7dfd54"
report_type="android"
f_flakefile="/tmp/flakey.txt"
wget -c https://raw.githubusercontent.com/tom-gall/android-qa-classifier-data/master/flakey.txt -O "${f_flakefile}"
f_report_config="${LKFT_WORK_DIR}/squad-report-config.yml"
wget -O "${f_report_config}" "https://gitlab.com/Linaro/lkft/pipelines/lkft-common/-/raw/master/squad-report-config.yml"

# commits="5.15.41-885990cd85e7 5.15.41-8a410d778a3c 5.15.41-8508df8a8452 5.15.41-d79ff7f93154 5.15.41-ed6b49858777 5.15.41-409a65243497 5.15.41-d559034f2011 "
commits="${qareport_build}"
for commit in ${commits}; do
    ./squad-report/squad-report \
                  --url=https://qa-reports.linaro.org \
                  --report-kernel ${report_kernel} \
                  --build=${commit} \
                  --config-report-type=android \
                  --config squad-report-config.yml \
                  --flakefile /tmp/flakey.txt \
                  --trim-number 1000 \
                  --output android-android-android13-5.15-${commit}.txt

done

free -h
df -h 

cd ${dir_old}
swapoff swapfile2
rm swapfile2

free -h
df -h 
