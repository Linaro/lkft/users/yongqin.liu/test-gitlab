#!/bin/bash -ex

# shellcheck source=/dev/null
source "${DIR_SCRIPTS_PATH}/lkft-common-bash-function.sh"
###########################################################
kernel_branch="${1}"

kernel_label="$(get_kernel_describe)"

curl -fsSL https://raw.githubusercontent.com/Linaro/android-report/master/deploy.sh -o deploy.sh && chmod +x deploy.sh

dir_root_android_report="/android/django_instances"
mkdir -p "${dir_root_android_report}"
./deploy.sh "${dir_root_android_report}" false
if [ -n "${QA_REPORTS_TOKEN}" ]; then
    sed -i "s|qareport_token = \"\" ## TO BE UPDATED|qareport_token = \"${QA_REPORTS_TOKEN}\" ## TO BE UPDATED|" ${dir_root_android_report}/android-report/lcr/settings.py
fi
f_report="/tmp/kernelreport-${kernel_branch}-${kernel_label}.txt"
for i in {1..5}; do
    echo "Try for the $i times to generate the report"
    ${dir_root_android_report}/android-report/kernelreport.sh "${kernel_branch}" --exact-version-1 "${kernel_label}" --no-check-kernel-version
    if [ -f "${f_report}" ]; then
        break
    fi
done

if [ ! -f "${f_report}" ]; then
    echo "Tried to generate the report for kernel: ${kernel_branch} ${kernel_label} but failed" > "${f_report}"
fi

regressions_number=1000
if [ -n "${REGRESSIONS_NUMBER_TO_BE_CHECKED}" ]; then
    regressions_number="${REGRESSIONS_NUMBER_TO_BE_CHECKED}"
fi

found_huge_regressions=false
# shellcheck disable=SC1004
if grep 'Regressions of' "${f_report}" | grep '^ ' | \
    awk -v regressions_number="${regressions_number}" \
        'BEGIN { huge_regressions=0; } \
        {if ($1 + 0 > regressions_number) {huge_regressions=huge_regressions + 1;}} \
        END {if ( huge_regressions >= 1 ){ exit 0;} else { exit 1; }}'; then
    found_huge_regressions=true
fi

found_failed_project=false
if grep 'NOTE: project' "${f_report}"; then
    found_failed_project=true
fi

if ${found_failed_project} || $found_huge_regressions; then
    # send to REVIEW_CC_ISSUE when not empty, otherwise send to REVIEW_TO
    if [ -n "${REVIEW_TO_ISSUE}" ]; then
        export REVIEW_TO_NAME="REVIEW_TO_ISSUE"
    fi
    # send to REVIEW_CC_ISSUE when not empty, otherwise send to REVIEW_CC
    if [ -n "${REVIEW_CC_ISSUE}" ]; then
        export REVIEW_CC_NAME="REVIEW_CC_ISSUE"
    fi
    msg_subject="[lkft-android-report] [issue] ${CI_PROJECT_NAME} | ${kernel_branch} | ${kernel_label}"
else
    msg_subject="[lkft-android-report] ${CI_PROJECT_NAME} | ${kernel_branch} | ${kernel_label}"
fi

send_mail_common "${f_report}" "${msg_subject}"
