#!/bin/bash

##########functions definition########################
# needs to be run within the docker container of squadproject/squad-report:1.2.0
# which has the build-email and send-mail command installed by default
function send_mail_common(){
    local f_email_body="${1}"
    local email_subject="${2}"

    if [ -n "${REVIEW_TO_NAME}" ]; then
        REAL_REVIEW_TO=$(eval "echo \$${REVIEW_TO_NAME}")
        if [ -n "${REAL_REVIEW_TO}" ]; then
            export REVIEW_TO="${REAL_REVIEW_TO}"
        fi
    fi

    if [ -n "${REVIEW_CC_NAME}" ]; then
        REAL_REVIEW_CC=$(eval "echo \$${REVIEW_CC_NAME}")
        if [ -n "${REAL_REVIEW_CC}" ]; then
            export REVIEW_CC="${REAL_REVIEW_CC}"
        fi
    fi

    build-email \
        --msg-from="${REVIEW_FROM}" \
        --msg-to="${REVIEW_TO}" \
        --msg-cc="${REVIEW_CC}" \
        --msg-body="${f_email_body}" \
        --msg-subject="${email_subject}" \
        --msg-body-pre="Go here to check the pipeline jobs: ${CI_PIPELINE_URL}." >mail.txt

    send-email mail.txt
}

# shellcheck disable=SC2120
function get_kernel_version(){
    local f_kernel_makefile="${1}"
    if [ -z "${f_kernel_makefile}" ]; then
        f_kernel_makefile="Makefile"
    fi
    if [ -n "${MAKE_KERNELVERSION}" ]; then
        kernel_version="${MAKE_KERNELVERSION}"
    elif [ -f "${f_kernel_makefile}" ]; then
        # parse the Makefile directory
        # KERNELVERSION = $(VERSION)$(if $(PATCHLEVEL),.$(PATCHLEVEL)$(if $(SUBLEVEL),.$(SUBLEVEL)))$(EXTRAVERSION)
        head -n5 "${f_kernel_makefile}" |sed 's| =|=|' | sed 's|= |=|' > "${f_kernel_makefile}.tmp"
        # shellcheck source=/dev/null
        kernel_version=$(source "${f_kernel_makefile}.tmp"; echo "${VERSION}${PATCHLEVEL:+.${PATCHLEVEL}${SUBLEVEL:+.${SUBLEVEL}}}${EXTRAVERSION}")
    elif [ -f "${F_KERNEL_INFO}" ]; then
        # this file is generated by the new_changes_notification job
        kernel_version=$(jq -r '.MAKE_KERNELVERSION' "${F_KERNEL_INFO}")
    else
        f_build_info=""
        if [ -n "${ANDROID_BUILD_CONFIG}" ] && [ -f "${ANDROID_BUILD_CONFIG}-build.json" ]; then
            f_build_info="${ANDROID_BUILD_CONFIG}-build.json"
        elif [ -n "${KERNEL_BUILD_CONFIG}" ] && [ -f "${KERNEL_BUILD_CONFIG}-build.json" ]; then
            f_build_info="${KERNEL_BUILD_CONFIG}-build.json"
        fi
        if [ -f "${f_build_info}" ]; then
            if grep -q -s MAKE_KERNELVERSION "${f_build_info}"; then
                # file that from the jenkins build
                kernel_version=$(jq .MAKE_KERNELVERSION "${f_build_info}" |tr -d \")
            elif grep -q -s kernel_version "${f_build_info}"; then
                # file that generated by the tuxsuite service
                kernel_version=$(jq -r '.[] | .kernel_version' "${f_build_info}")
            fi
        fi
    fi

    if [ -z "${kernel_version}" ]; then
        kernel_version="unkonwn"
    fi
    echo "${kernel_version}"
}

function get_kernel_branch(){
    if [ -n "${KERNEL_BRANCH}" ]; then
        echo "${KERNEL_BRANCH}"
    else
        echo "${CI_BUILD_REF_NAME:-unknown}"
    fi
}

## KERNEL_COMMIT and SRCREV_kernel could be defined as global environment variables
function get_kernel_commit(){
    # shellcheck disable=SC2153
    if [ -n "${KERNEL_COMMIT}" ]; then
        echo "${KERNEL_COMMIT}"
    elif [ -n "${SRCREV_kernel}" ]; then
        echo "${SRCREV_kernel}"
    else
        echo "${CI_COMMIT_SHA:-unknown}"
    fi
}

function get_kernel_describe(){
    local kernel_describe=""
    if [ -n "${KERNEL_DESCRIBE}" ]; then
        kernel_describe="${KERNEL_DESCRIBE}"
    else
        local kernel_version="$(get_kernel_version)"
        local kernel_commit="$(get_kernel_commit)"
        kernel_describe="${kernel_version}-${kernel_commit:0:12}"
    fi
    echo "${kernel_describe}"
}

function clean_and_backup_ssh_files(){
    for f in "$@"; do
        rm -fr "${HOME}/.ssh/${f}-backup-gitlab"
        if [ -f "${HOME}/.ssh/${f}" ]; then
            cp -v "${HOME}/.ssh/${f}" "${HOME}/.ssh/${f}-backup-gitlab"
        fi
    done
}

function restore_ssh_files(){
    for f in "$@"; do
        if [ -f "${HOME}/.ssh/${f}-backup-gitlab" ]; then
            mv -f "${HOME}/.ssh/${f}-backup-gitlab" "${HOME}/.ssh/${f}"
        else
            # which means there was no file to be backuped before
            rm -fr "${HOME}/.ssh/${f}"
        fi
    done
}

function setup_lkft_bot_key(){
    if [ -n "${DEV_PRIVATE_GIT}" ] && [ "${DEV_PRIVATE_GIT}" = "True" ]; then
        mkdir -p "${HOME}/.ssh" && chmod 700 "${HOME}/.ssh"
        clean_and_backup_ssh_files config known_hosts

        ssh-keyscan dev-private-git.linaro.org >> "${HOME}/.ssh/known_hosts"

        rm -fr "${HOME}/.ssh/lkft-bot"
        cp "${LKFT_BOT_KEY_FILE}" "${HOME}/.ssh/lkft-bot"
        chmod 600 "${HOME}/.ssh/lkft-bot"

        cat "${LKFT_BOT_CONFIG_FILE}" >> "${HOME}/.ssh/config"
        chmod 600 "${HOME}/.ssh/config"
    fi
}

function clean_lkft_bot_key(){
    if [ -n "${DEV_PRIVATE_GIT}" ] && [ "${DEV_PRIVATE_GIT}" = "True" ]; then
        rm -fr "${HOME}/.ssh/lkft-bot"
        restore_ssh_files config known_hosts
    fi
}

function find_callback_job_url_from_pipeline(){
    local callback_job_name="${1}"
    local project_id="${2}"
    local pipeline_id="${3}"
    if [ -z "${callback_job_name}" ]; then
        return
    fi

    if [ -z "${project_id}" ]; then
        project_id="${CI_PROJECT_ID}"
    fi
    if [ -z "${pipeline_id}" ]; then
        pipeline_id="${CI_PIPELINE_ID}"
    fi

    # NOTE1:
    #   https://docs.gitlab.com/ee/api/jobs.html
    #   when all the callback jobs finished, there will be no jobs returned with "scope[]=manual" specified
    #   scope is the status, will be changed when the job is running or finished, etc
    #   here does not specify the scope to have all jobs returned, and the callback
    #   could be registered again, even it's called before
    # NOTE#2
    #   https://docs.gitlab.com/ee/api/index.html#pagination-link-header
    #   here set to use the max 100 for the per_page, assuming there won't be pipeline that has more than 100 jobs
    #   For that case, we need either update here or re-design the pipeline jobs
    local f_pipeline_jobs="pipelines-jobs-${pipeline_id}.json"
    if [ -n "${ACCESS_TO_GITLAB_PRIVATE_PIPELINES}" ] && [ "${ACCESS_TO_GITLAB_PRIVATE_PIPELINES}" = "True" ] && [ -n "${GITLAB_TOKEN}" ]; then
        curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${project_id}/pipelines/${pipeline_id}/jobs?per_page=100" -o "${f_pipeline_jobs}"
    else
        curl "https://gitlab.com/api/v4/projects/${project_id}/pipelines/${pipeline_id}/jobs?per_page=100" -o "${f_pipeline_jobs}"
    fi
    job_id=$(jq -r ".[] | select(.name == \"${callback_job_name}\") | .id" "${f_pipeline_jobs}")
    callback_url="https://gitlab.com/api/v4/projects/${project_id}/jobs/${job_id}/play"
    echo "${callback_url}"
}

## DEPRECATED, please use find_callback_job_url_from_pipeline directly
function find_callback_job_url_from_current_pipeline(){
    find_callback_job_url_from_pipeline "$@"
}

## common function to get the gki kernel url from the gki build.json file
## Note: gki_build_config needs to be checked before this call to make sure it's not empty
function get_gki_image_gz_url(){
    local gki_build_config="${1}"
    local f_gki_build_json="${gki_build_config}-build.json"
    local snapshot_download_url=""
    if [ -f "${f_gki_build_json}" ]; then
        snapshot_download_url=$(jq -r .SNAPSHOT_DOWNLOAD_URL "${f_gki_build_json}")
    fi
    if [ -n "${snapshot_download_url}" ]; then
        echo "${snapshot_download_url}/${gki_build_config}-Image.gz"
    fi
}
######### functions definiton end here ####################################
