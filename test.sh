#!/bin/bash -e

apt-get update && apt-get install -y git awscli

function clean_and_backup_ssh_files(){
    for f in "$@"; do
        rm -fr "${HOME}/.ssh/${f}-backup-gitlab"
        if [ -f "${HOME}/.ssh/${f}" ]; then
            cp -v "${HOME}/.ssh/${f}" "${HOME}/.ssh/${f}-backup-gitlab"
        fi
    done
}

function restore_ssh_files(){
    for f in "$@"; do
        if [ -f "${HOME}/.ssh/${f}-backup-gitlab" ]; then
            mv -f "${HOME}/.ssh/${f}-backup-gitlab" "${HOME}/.ssh/${f}"
        else
            # which means there was no file to be backuped before
            rm -fr "${HOME}/.ssh/${f}"
        fi
    done
}

function setup_lkft_bot_key(){
    if [ -n "${DEV_PRIVATE_GIT}" ] && [ "${DEV_PRIVATE_GIT}" = "True" ]; then
        mkdir -p "${HOME}/.ssh" && chmod 700 "${HOME}/.ssh"
        clean_and_backup_ssh_files config known_hosts

        ssh-keyscan dev-private-git.linaro.org >> "${HOME}/.ssh/known_hosts"

        rm -fr "${HOME}/.ssh/lkft-bot"
        cp "${LKFT_BOT_KEY_FILE}" "${HOME}/.ssh/lkft-bot"
        chmod 600 "${HOME}/.ssh/lkft-bot"

        cat "${LKFT_BOT_CONFIG_FILE}" >> "${HOME}/.ssh/config"
        chmod 600 "${HOME}/.ssh/config"
    fi
}

function clean_lkft_bot_key(){
    if [ -n "${DEV_PRIVATE_GIT}" ] && [ "${DEV_PRIVATE_GIT}" = "True" ]; then
        rm -fr "${HOME}/.ssh/lkft-bot"
        restore_ssh_files config known_hosts
    fi
}

function access_to_dev_private_git(){
    export DEV_PRIVATE_GIT=True
    setup_lkft_bot_key
    git clone -b lkft git@dev-private-git.linaro.org:android-internal/android-build-configs android-build-configs-private
    cat android-build-configs-private/lkft/lkft-db845c-android12-android12-5.10-gki
    clean_lkft_bot_key
    unset DEV_PRIVATE_GIT
}

function access_to_snapshot_protected_files(){
    echo "Try to copy protected files from snapshot"
    set -x
    ##====
    #aws s3 cp s3://publishing-ie-linaro-org/snapshots/android/lkft/protected/lkft-member-build-gitlab/1/lkft-db845c-android12-android12-5.10-gki-misc_info.txt ./lkft-db845c-android12-android12-5.10-gki-misc_info.txt
    #echo "content of lkft-db845c-android12-android12-5.10-gki-misc_info.txt====="
    #cat ./lkft-db845c-android12-android12-5.10-gki-misc_info.txt
    #echo "content of lkft-db845c-android12-android12-5.10-gki-misc_info.txt====="

    # https://validation-linaro-org.s3.amazonaws.com/testdata/lkft/aosp-stable/android/lkft/protected/aosp/android-cts/175/build_fingerprint.txt
    #aws s3 cp s3://validation-linaro-org/testdata/lkft/aosp-stable/android/lkft/protected/aosp/db845c/173/build_fingerprint.txt .
    ## http://testdata.linaro.org/lkft/aosp-stable/android/lkft/protected/aosp/android-cts/175/build_fingerprint.txt
    aws s3 cp s3://validation-linaro-org/testdata/lkft/aosp-stable/android/lkft/protected/aosp/android-cts/175/build_fingerprint.txt ./cts-175-build_fingerprint.txt
    cat cts-175-build_fingerprint.txt
    set +x
}

#access_to_dev_private_git
access_to_snapshot_protected_files
